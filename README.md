# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Projeto criado com o objetivo de realizar um desafio proposto pela SKY.
Contém uma tela, onde faz a requisição de um serviço que retorna uma lista de filmes a ser exibida.

Versão 1.0

### How do I get set up? ###

Faça o clone do projeto;
Abra o terminal e rode os comandos para instalar os frameworks:

cd caminho/do/repositorio
pod install

Abra o projeto no Xcode a partir do arquivo SkyExercise.xcworkspace criado na pasta origem do repositório.

### Who do I talk to? ###

Felippe George Bertges de Amorim
bertges.felippe@gmail.com