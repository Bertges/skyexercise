//
//  MovieCollectionCell.swift
//  SkyExercise
//
//  Created by Felippe Bertges on 23/02/2018.
//  Copyright © 2018 Sky. All rights reserved.
//

import UIKit

class MovieCollectionCell: UICollectionViewCell {
    
    //MARK: - Outlets
    
    @IBOutlet weak var imgMovie: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    //MARK: - Properties
    
    static let nib = UINib(nibName: "MovieCollectionCell", bundle: Bundle.main)
    static let identifier = "MovieCollectionCell"

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    /// Configura uma instancia de MovieCollectionCell a partir do MovieViewModel
    ///
    /// - Parameters:
    ///   - collectionView: collectionView onde a cell será inserida
    ///   - indexPath: indexPath da cell
    ///   - viewModel: MovieViewModel usado para configurar a cell
    /// - Returns: A instancia de MovieCollectionCell criada
    static func instance(collectionView: UICollectionView, cellForItemAt indexPath: IndexPath, viewModel: MovieViewModel) -> MovieCollectionCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.identifier, for: indexPath) as! MovieCollectionCell
        
        cell.lblTitle.text = viewModel.title
        cell.imgMovie.layer.cornerRadius = 5
        
        guard let url = viewModel.coverUrl else {
            return cell
        }
        
        cell.imgMovie.af_setImage(withURL: url, placeholderImage: MovieViewModel.imgCoverNotFound, filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: true, completion: nil)
        
        return cell
    }

}
