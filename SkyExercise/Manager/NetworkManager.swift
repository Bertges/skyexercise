//
//  NetworkManager.swift
//  SkyExercise
//
//  Created by Felippe Bertges on 23/02/2018.
//  Copyright © 2018 Sky. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

enum ResponseResult {
    case success(Any),
    failure(Error?)
}

class NetworkManager {
    
    //MARK: - Base Request
    
    /// Metodo genérico para requisições de serviço
    ///
    /// - Parameters:
    ///   - url: url do serviço sendo chamado
    ///   - completion: bloco de resposta do serviço
    static func request(_ url: URL, completion: @escaping (_ respose: ResponseResult) -> Void) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        Alamofire.request(url).responseJSON { (response) in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            switch response.result {
            case .success:
                if let result = response.result.value {
                    completion(.success(result))
                } else {
                    completion(.failure(nil))
                }
                break
            case .failure(let error):
                completion(.failure(error))
                break
            }
        }
    }
    
    //MARK: - Movies
    
    /// Chamada do serviço que lista os filmes
    ///
    /// - Parameter completion: bloco de resposta do serviço
    static func fetchMovies(completion: @escaping (_ response: [Movie], _ success: Bool) -> Void) {
        let url = URL(string: "https://sky-exercise.herokuapp.com/api/Movies")!
        self.request(url) { (response) in
            switch response {
            case .success(let result):
                if let movies = Mapper<Movie>().mapArray(JSONObject: result) {
                    completion(movies, true)
                    return
                }
            default:
                break
            }
            
            
            completion([], false)
        }
    }
    
}
