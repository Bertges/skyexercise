//
//  Movie.swift
//  SkyExercise
//
//  Created by Felippe Bertges on 23/02/2018.
//  Copyright © 2018 Sky. All rights reserved.
//

import Foundation
import ObjectMapper

class Movie: Mappable {
    
    //MARK: - Properties
    
    var title: String = ""
    var overview: String = ""
    var duration: String = ""
    var releaseYear: String = ""
    var coverUrl: String = ""
    var backdropsUrl: String = ""
    var id: String = ""
    
    //MARK: - Inits
    
    /// Inicializacao padrao do objeto Movie
    init() {
        self.title = ""
        self.overview = ""
        self.duration = ""
        self.releaseYear = ""
        self.coverUrl = ""
        self.backdropsUrl = ""
        self.id = ""
    }
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    /// Serializa objeto Movie
    ///
    /// - Parameter map: map do Json de Movie
    func mapping(map: Map) {
        self.title          <- map["title"]
        self.overview       <- map["overview"]
        self.duration       <- map["duration"]
        self.releaseYear    <- map["release_year"]
        self.coverUrl       <- map["cover_url"]
        self.backdropsUrl   <- map["backdrops_url"]
        self.id             <- map["id"]
    }
}
