//
//  ViewController.swift
//  SkyExercise
//
//  Created by Felippe Bertges on 23/02/2018.
//  Copyright © 2018 Sky. All rights reserved.
//

import UIKit
import AlamofireImage

enum ViewState {
    case loading,
    failed,
    loaded
}

class MovieListViewController: UIViewController {
    
    //MARK: - Actions
    
    @IBOutlet weak var btnReload: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    
    //MARK: - Properties
    
    var movieListViewModel = MovieListViewModel()
    var viewState: ViewState = .loading {
        didSet {
            self.didUpdateViewState()
        }
    }
    private let numberOfColumns: CGFloat = 2
    private let ratio:CGFloat = 1.60

    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Cine SKY"
        self.setupCollectionView()
        self.requestMovies()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - User Interface
    
    /// Atualiza a view de acordo com o estado atual
    private func didUpdateViewState() {
        switch self.viewState {
        case .loading:
            self.collectionView.isHidden = true
            self.btnReload.isHidden = true
            self.loading.startAnimating()
            break
            
        case .failed:
            self.collectionView.isHidden = true
            self.btnReload.isHidden = false
            self.loading.stopAnimating()
            break
            
        case .loaded:
            self.collectionView.reloadData()
            self.collectionView.isHidden = false
            self.btnReload.isHidden = true
            self.loading.stopAnimating()
            break
        }
    }
    
    //MARK: - Actions

    /// Ação do botão de recarregar, disponível quando o carregamento falha
    ///
    /// - Parameter sender: botão que disparou a ação
    @IBAction func reload(_ sender: UIButton) {
        self.requestMovies()
    }
    
    //MARK: - Request
    
    /// Trata a chamada do serviço de filmes
    private func requestMovies() {
        self.viewState = .loading
        self.movieListViewModel.fetch { (success) in
            if success {
                self.viewState = .loaded
            } else {
                self.viewState = .failed
            }
        }
    }
    
    //MARK: - CollectionView

    /// Registra as cells usadas na collectionView
    private func setupCollectionView() {
        self.collectionView.register(MovieCollectionCell.nib, forCellWithReuseIdentifier: MovieCollectionCell.identifier)
    }
}

//MARK: - UICollectionViewDataSource

extension MovieListViewController: UICollectionViewDataSource  {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.movieListViewModel.list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let movieViewModel = self.movieListViewModel.list[indexPath.item]
        return MovieCollectionCell.instance(collectionView: collectionView, cellForItemAt: indexPath, viewModel: movieViewModel)
    }
}

//MARK: - UICollectionViewDelegateFlowLayout

extension MovieListViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.size.width / self.numberOfColumns) - 10, height: (collectionView.frame.size.width / 2) * self.ratio)
    }
}

