//
//  MovieListViewModel.swift
//  SkyExercise
//
//  Created by Felippe Bertges on 23/02/2018.
//  Copyright © 2018 Sky. All rights reserved.
//

import Foundation

class MovieListViewModel {
    
    //MARK: - Properties
    
    var list: [MovieViewModel] = []
    
    //MARK: - Init
    
    init() {
    }
    
    //MARK: - Request
    
    func fetch(completion: @escaping (_ success: Bool) -> Void) {
        NetworkManager.fetchMovies { (movieList, success) in
            for movie in movieList {
                self.list.append(MovieViewModel(movie))
            }
            completion(success)
        }
    }
}
