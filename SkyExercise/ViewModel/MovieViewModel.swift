//
//  MovieViewModel.swift
//  SkyExercise
//
//  Created by Felippe Bertges on 23/02/2018.
//  Copyright © 2018 Sky. All rights reserved.
//

import Foundation
import UIKit

class MovieViewModel {
    
    //MARK: - Properties
    
    private let movie: Movie!
    
    var title: String {
        get {
            return movie.title
        }
    }
    
    var coverUrl: URL? {
        get {
            return URL(string: self.movie.coverUrl)
        }
    }
    
    static let imgCoverNotFound = UIImage(named: "imageNotFound") ?? UIImage()
    
    //MARK: - Inits
    
    init(_ movie: Movie) {
        self.movie = movie
    }
}
