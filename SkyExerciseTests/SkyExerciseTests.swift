//
//  SkyExerciseTests.swift
//  SkyExerciseTests
//
//  Created by Felippe Bertges on 23/02/2018.
//  Copyright © 2018 Sky. All rights reserved.
//

import XCTest
@testable import SkyExercise
import Nimble

class SkyExerciseTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testRequestMovieListSerialization() {
        let movieListViewModel = MovieListViewModel()
        
        movieListViewModel.fetch { (success) in
        }
        
        expect(movieListViewModel.list.count).toEventually(beGreaterThan(0), timeout: 100, pollInterval: 0.5, description: "Verifica se o serviço retorna uma lista de filmes")
    }
    
    func testMovieListContainsMovieWithTitle() {
        let movieTitle = "Doutor Estranho"
        
        var hasExpectedMovie: Bool = false
        let movieListViewModel = MovieListViewModel()
        
        movieListViewModel.fetch { (success) in
            if success {
                var moviesTitles: [String] = []
                moviesTitles = movieListViewModel.list.map { $0.title }
                
                if moviesTitles.contains(movieTitle) {
                    hasExpectedMovie = true
                }
            }
        }
        
        expect(hasExpectedMovie).toEventually(equal(true), timeout: 10, pollInterval: 0.5, description: "Verifica se a lista retornada do serviço contém um filme com o título esperado.")
    }
    
    func testServicePerformance() {
        var returned: Bool = false
        let movieListViewModel = MovieListViewModel()
        
        movieListViewModel.fetch { (success) in
            returned = true
        }
        
        expect(returned).toEventually(equal(true), timeout: 5, pollInterval: 0.5, description: "Realiza a chamada do serviço de filmes, e verifica se retorna dentro do tempo esperado.")
    }
    
}
